const express = require("express")
const path = require('path')

const app = express()
const userRouter = require('./routes/userRoutes')
const viewRouter = require('./routes/viewRoutes')
const cookieParser = require('cookie-parser')
app.use(cookieParser())

app.use(express.json())
app.use('/api/v1/users', userRouter)
app.use('/', viewRouter)


// const express = require('express')
const mongoose = require('mongoose')
const Article = require('./models/article')
const articleRouter = require('./routes/articles')
const methodOverride = require('method-override')
// const app = express()

// mongoose.connect('mongodb://localhost/blog', {
//   useUnifiedTopology: true
// })

// mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true })
// const db = mongoose.connection
// db.on('error', error => console.error(error))
// db.once('open', () => console.log('Connected to Mongoose'))



app.set('view engine', 'ejs')
app.use(express.urlencoded({ extended: false }))
app.use(methodOverride('_method'))


app.get('/community', async (req, res) => {
    const articles = await Article.find().sort({ createdAt: 'desc' })
    res.render('articles/index', { articles: articles })
})

app.use('/articles', articleRouter)



app.use(express.static(path.join(__dirname, 'views')))

module.exports = app